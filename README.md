SilexEvolutionKitchen
=====================

This project is a sample or a bootstrap silex application. You can use it for your next php application.

- bootstrap 3.3.4
- font awesome 4.2.0
- bootstrap datetime picker (disabled)
- summernote

The following directories must be writable
web/assets
web/assets/css
web/assets/js 
resources/cache 
resources/log
