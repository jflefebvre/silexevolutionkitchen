<?php

namespace Evolution\Silex\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

use Evolution\Silex\PDF;

/**
 * PDF Service Provider.
 *
 * @author Jean-François Lefebvre <lefebvre.jf@gmail.com>
 */
class PDFServiceProvider implements ServiceProviderInterface
{


	public function register(Application $app)
    {
    	$app['pdf'] = $app->share(function ($app) {
    		$pdf = new PDF($app);

    		return $pdf;
    	});
    }

    public function boot(Application $app)
    {
    }


}