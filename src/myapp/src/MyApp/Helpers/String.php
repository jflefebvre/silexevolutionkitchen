<?php

namespace MyApp\Helpers;

class String {
	
	static public function utf8_encode($array) {
	    array_walk_recursive($array, function(&$item, $key){
	        if(!mb_detect_encoding($item, 'utf-8', true)){
	                $item = utf8_encode($item);
	        }
	    });
	 
	    return $array;
	}

	static public function utf8_decode($array) {
	    array_walk_recursive($array, function(&$item, $key){
	        if(mb_detect_encoding($item, 'utf-8', true)){
	                $item = utf8_decode($item);
	        }
	    });
	 
	    return $array;
	}

}