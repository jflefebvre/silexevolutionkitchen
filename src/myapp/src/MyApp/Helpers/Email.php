<?php

namespace MyApp\Helpers;

/**
 *
 *	Provides email related functions
 *  
 *  @author Jean-François Lefebvre <jf.lefebvre@2bcom.eu>
 */

class Email {


	/**
	 *	Send e-mail with PHP mailer
	 *
	 *  @param Application
	 *	@param array  emails (to). 
	 *	@param array  emails (cc). 
	 *	@param array  emails (bcc). 
	 *	@param array  emails (ReplyTo). 
	 *	@param string Fullname displayed in email client
	 *	@param string email Subject:
	 *	@param string HTML message to send
	 *	@param string Alternate text message
	 * 
	 *	@return boolean 
	 */

	static public function sendmail($app,  
							 $emailToAddresses, 
							 $emailCcAddresses,
							 $emailBccAddresses,
							 $emailReplyToAddresses, 
							 $emailSubject, 
							 $htmlMessage, 
							 $textMessage = '') {

		// $app['monolog']->addInfo('Helpers::sendmail()');

		$config = $app['phpmailer'];
		
		$mail = new \PHPMailer();

		$mail->CharSet = "UTF-8";

		if ($config['smtp_debug']) {
			$mail->SMTPDebug = 3;
		}
		$mail->IsSMTP();                        // set mailer to use SMTP
		$mail->Host = $config['smtp_server'];  	// specify main and backup server
		$mail->Port = $config['smtp_port'];
		$mail->SMTPAuth = true;     			// turn on SMTP authentication
		$mail->Username = $config['username'];  	// SMTP username
		$mail->Password = $config['password']; 	// SMTP password
		
		$mail->From = $config['username'];
		$mail->FromName = $config['username_full'];
		
		// custom headers
		/*
		$mail->addCustomHeader("Reply-To: The Sender <".$config->username.">");
		$mail->addCustomHeader("Return-Path: The Sender <".$config->username.">");
		$mail->addCustomHeader("From: The Sender <".$config->username.">");
		$mail->addCustomHeader("X-Priority: 3");
		$mail->addCustomHeader("X-Mailer: PHP". phpversion());
		*/

		foreach ($emailToAddresses as $emailAddress) {
			$mail->AddAddress($emailAddress['email'], $emailAddress['email_name']);	
		}

		foreach ($emailCcAddresses as $emailAddress) {
			$mail->addCC($emailAddress['email'], $emailAddress['email_name']);	
		}	

		foreach ($emailBccAddresses as $emailAddress) {
			$mail->addBCC($emailAddress['email'], $emailAddress['email_name']);	
		}

		foreach ($emailReplyToAddresses as $emailAddress) {
			$mail->AddReplyTo($emailAddress['email'], $emailAddress['email_name']);	
		}

		$mail->WordWrap = 50;                                 // set word wrap to 50 characters
		//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
		//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
		$mail->IsHTML(true);                                  // set email format to HTML

		$mail->Subject = $emailSubject;
		$mail->Body    = $htmlMessage;
		$mail->AltBody = $textMessage;
		
		/*
		if ($config->debug) {
			$app['monolog']->addInfo('mode debug is active - no e-mail sent');
			return true;
		}
		*/

		$smtp_debug = '';
		
		if ($config['smtp_debug']) {
			ob_start();
		}

		if(!$mail->Send()) {
		 	if ($config['smtp_debug']) {
				$smtp_debug = ob_get_contents();
				ob_end_clean();
		 		$app['monolog']->addInfo($mail->ErrorInfo);
		 		$app['monolog']->addInfo($smtp_debug);
		 	}
		    return false;
		}

		if ($config['smtp_debug']) {
			$smtp_debug = ob_get_contents();
			ob_end_clean();
			$app['monolog']->addInfo($smtp_debug);
		}

		return true;
	}


}