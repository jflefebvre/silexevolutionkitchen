<?php

namespace MyApp\Helpers;

class Security {

	/**
	 *
	 * @return returns a random number used as salt for password
	 */
	static public function getSalt() {
		return uniqid(mt_rand(), true);
	}
	
	/**
	 * 
	 *
	 * @return string returns string containing the sha512 of the contactenation of the password and salt value
	 */
	static public function getHash($password, $salt) {
		return hash('sha512', $password . $salt);
	}
	
}