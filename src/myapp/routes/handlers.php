<?php

use Symfony\Component\HttpFoundation\Request;

$app->before(function (Request $request) use ($app) {
    
    $locale = $app['request']->get('lang');
  
    if ($locale == null) {
        $locale = $app['session']->get('_locale');
    } 

    if (!in_array($locale, array('fr', 'nl', 'en'))) {
        $locale = 'en';
    }
    $app['locale'] = $locale;
    $app['session']->set('_locale', $locale);

    if (!$app['session']->has('IS_AUTHENTICATED_FULLY') && strpos($request->getRequestUri(), '/login') === FALSE) {
        $login = $app['url_generator']->generate('login');          
        return $app->redirect($login);
    }

});

$app->error(function (\Exception $e, $code) use ($app) {
    
    if ($app['debug']) {
        return;
    }

    switch ($code) {
        case 404:
            $message = 'The requested page could not be found.';
            break;
        default:
            $message = 'We are sorry, but something went terribly wrong.';
    }

    return new Response($message, $code);
});
