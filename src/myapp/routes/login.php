<?php

use Symfony\Component\HttpFoundation\Request;

$app->match('/login', function (Request $request) use ($app) {
    $form = $app['form.factory']->createBuilder('form')
        ->add(
            'username',
            'email',
            array(
                'label' => 'E-mail',
                'data' => '', 
                'attr'=> array('class' => 'form-control medium-width', 'placeholder' => 'E-mail')
            )
        )
        ->add('password', 
              'password', 
            array(
                'label' => 'Password', 
                'attr'=> array('class' => 'form-control medium-width', 'placeholder' => 'Password')))
        ->getForm();

    return $app['twig']->render('login.html.twig', array(
        'form'  => $form->createView()
    ));

})->bind('login');

$app->match('/login_check', function (Request $request) use ($app) {
    
    $app['session']->set('IS_AUTHENTICATED_FULLY', 1);
    $dashboard = $app['url_generator']->generate('dashboard');   

    return $app->redirect($dashboard);
})->bind('login_check');

$app->match('/logout', function () use ($app) {
    $app['session']->clear();

    return $app->redirect($app['url_generator']->generate('dashboard'));
})->bind('logout');
