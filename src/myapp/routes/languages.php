<?php

$app->get('/switch/{locale}', function ($locale) use ($app) {
	$app['session']->set('_locale', $locale);
	//echo $app['session']->get('_locale');die();
	//$app['session.default_locale'] = $locale;
	$dashboard = $app['url_generator']->generate('dashboard', array());
	return $app->redirect($dashboard.'?lang='.$locale);
})->assert('locale', 'fr|nl|en')
->value('locale', 'en')
->bind('locale');