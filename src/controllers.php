<?php

use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

include  __DIR__.'/myapp/routes/login.php';
include  __DIR__.'/myapp/routes/languages.php';
include  __DIR__.'/myapp/routes/dashboard.php';

include  __DIR__.'/myapp/routes/database.php';

include  __DIR__.'/myapp/routes/handlers.php';

return $app;
