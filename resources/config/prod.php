<?php

// Local
$locale = 'en';

$app['locale'] = $locale;
$app['session.default_locale'] = $app['locale'];
$app['translator.messages'] = array(
    'fr' => __DIR__.'/../resources/locales/fr.yml',
    'en' => __DIR__.'/../resources/locales/en.yml',
    'nl' => __DIR__.'/../resources/locales/nl.yml',
);

// Cache
$app['cache.path'] = __DIR__ . '/../cache';

// Http cache
$app['http_cache.cache_dir'] = $app['cache.path'] . '/http';

// Twig cache
$app['twig.options.cache'] = $app['cache.path'] . '/twig';

// Assetic
$app['assetic.enabled']              = $app['debug'];
$app['assetic.path_to_cache']        = $app['cache.path'] . '/assetic' ;
$app['assetic.path_to_web']          = __DIR__ . '/../../assets';
$app['assetic.input.path_to_assets'] = __DIR__ . '/../assets';

$app['assetic.input.path_to_css']       = array(
    $app['assetic.input.path_to_assets'] . '/css/bootstrap-datetimepicker.css',
    $app['assetic.input.path_to_assets'] . '/css/summernote.css',
    $app['assetic.input.path_to_assets'] . '/css/summernote-bs3.css',
    $app['assetic.input.path_to_assets'] . '/css/custom.css'
);
$app['assetic.output.path_to_css']      = 'css/styles.css';

$app['assetic.input.path_to_js']        = array(
    $app['assetic.input.path_to_assets'] . '/js/moment-with-locales.js',
    $app['assetic.input.path_to_assets'] . '/js/bootstrap-datetimepicker.min.js',
    // $app['assetic.input.path_to_assets'] . '/js/bootstrap3-typehead.js',
    $app['assetic.input.path_to_assets'] . '/js/summernote.js',
    $app['assetic.input.path_to_assets'] . '/js/script.js',
);
$app['assetic.output.path_to_js']       = 'js/scripts.js';

$app['db.options'] = array();

if (@$_SERVER['HTTP_HOST'] == 'projectname.lab') {
    $app['db.options'] = array(
        'driver'   => 'pdo_mysql',
        'host'     => '127.0.0.1',
        'dbname'   => '',
        'user'     => 'root',
        'password' => '',
    );
} else {
    $app['db.options'] = array(
        'driver'   => 'pdo_mysql',
        'host'     => 'localhost',
        'dbname'   => '',
        'user'     => '',
        'password' => '',
    );
}
